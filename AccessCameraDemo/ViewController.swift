//
//  ViewController.swift
//  AccessCameraDemo
//
//  Created by Sengly Sun on 12/24/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func photoClicked(_ sender: Any) {
        createActionSheet()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        imageView.image = image
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK: - Open Camera
    func openCamera(){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .camera
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK: - Open photo library
    func photoLibrary(){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    //MARK: - ActionSheet
    func createActionSheet(){
        let alertController = UIAlertController(title: "Photo", message: nil, preferredStyle: .actionSheet)
        
        let photoLibray = UIAlertAction(title: "Photo Library", style: .default){(_) in
            self.photoLibrary()
        }
        
        let camera = UIAlertAction(title: "Open Camera", style: .default) { (_) in
            self.openCamera()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(photoLibray)
        alertController.addAction(camera)
        alertController.addAction(cancel)

        self.present(alertController, animated: true, completion: nil)
    }

}

